+-----------------------------------------------------------------------------+
|                                                                             |
|   Nome: André Akira Hayashi                                                 |
|   NUSP: 9293011                                                             |
|                                                                             |
|   Nome: Gustavo Mendes Maciel                                               |
|   NUSP: 9298062                                                             |
|                                                                             |
|   Nome: Vinicius Perche de Toledo Agostini                                  |
|   NUSP: 4367487                                                             |
|                                                                             |
+-----------------------------------------------------------------------------+

1. Tarefas cumpridas

    1.1. Apresentação

        +--------+---------------------------------------------+-------+
        | Código | Critério                                    | Valor |
        +--------+---------------------------------------------+-------+
        |   A1   | Atender o formato de entrega                |  +2   |
        |   A2   | Executar sem erros                          |  +3   |
        |   A3   | Explicar organização do código no relatório |  +3   |
        |   A4   | Listar tarefas cumpridas no relatório       |  +2   |
        +--------+---------------------------------------------+-------+

    1.2. Qualidade de código

        +--------+------------------------------------------------+-------+
        | Código | Critério                                       | Valor |
        +--------+------------------------------------------------+-------+
        |   U1   | Passar no luacheck                             |  +5   |
        |   U2   | Separação clara entre Model, View e os estados |  +3   |
        |   U5   | Linhas com até 100 caracteres                  |  +2   |
        +--------+------------------------------------------------+-------+

    1.3. Juiciness

        +--------+-----------------+-------+
        | Código | Critério        | Valor |
        +--------+-----------------+-------+
        |   J1   | Som no menu     |  +2   |
        |   J2   | Som nos ataques |  +2   |
        +--------+-----------------+-------+
       
    1.4. Combate básico

        +--------+------------------------------------------------------------+-------+
        | Código | Critério                                                   | Valor |
        +--------+------------------------------------------------------------+-------+
        |   B1   | Seleção de alvo para o ataque                              |  +2   |
        |   B2   | Ataque reduz a vida do alvo em uma quantidade fixa         |  +2   |
        |   B4   | Jogador vence o encontro quando elimina todos os oponentes |  +2   |
        |   B5   | Jogador falha a aventura se seus personagens morrerem      |  +2   |
        +--------+------------------------------------------------------------+-------+

    1.5. Turno dos inimigos

        +--------+--------------------------------------------------+-------+
        | Código | Critério                                         | Valor |
        +--------+--------------------------------------------------+-------+
        |   T1   | Inimigos escolhem suas próprias ações            |  +4   |
        |   T2   | Inimigos elegem alvos seguindo alguma prioridade |  +4   |
        +--------+--------------------------------------------------+-------+

    1.6. Estatísticas de combate

        +--------+-----------------------------------------+-------+
        | Código | Critério                                | Valor |
        +--------+-----------------------------------------+-------+
        |   E1   | Poder                                   |  +3   |
        |   E2   | Resistência                             |  +3   |
        |   E3   | Velocidade                              |  +6   |
        |   E4   | Carregar estatísticas do banco de dados |  +3   |
        +--------+-----------------------------------------+-------+

    1.7. Estatísticas de incerteza

        +--------+-----------------------------------------+-------+
        | Código | Critério                                | Valor |
        +--------+-----------------------------------------+-------+
        |   Z1   | Chance de acerto                        |  +3   |
        |   Z2   | Acertos críticos                        |  +3   |
        |   Z3   | Carregar estatísticas do banco de dados |  +6   |
        +--------+-----------------------------------------+-------+


2. Organização do código

    - a pasta assets/ contém o arquivo de mídia do jogo (áudio, texturas e
      fontes
    - a pasta common/ contém módulos e classes que são utilizadas pelo resto
      do código do jogo
    - a pasta database/ contém arquivos que especificam as entidades do jogo e
      suas propriedades
    - a pasta model/ contém módulos e classes que implementam a mecânica do
      jogo
    - a pasta state/ contém módulos que implementam os diversos estados de
      interação do jogo
    - a pasta view/ contém as classes dos elementos visuais do jogo, como
      interface gráfica (menus etc.) e a simulação visual do jogo

    1.1. Implementações feitas

        - foi criado um estado para representar as lutas (state/fight.lua),
          baseado nos arquivos de estado já implementados; esse estado é
          criado em cima do PlayerTurn na pilha
        - foi criada uma estrutura de dados para gerenciar a party (os
          personagens que o jogador controla) no arquivo state/encounter.lua e
          os oponentes que aparecem a cada encontro; isso serviu para poder
          direcionar os ataques
        - foram adicionados atributos aos personagens representados pelos
          arquivos .lua em database/characters
        - para implementar o combate básico foi incluído código nos arquivos
          model/character.lua (classe que representa um personagem), 
          state/encounter.lua (módulo responsável pelo estado de "encontro"
          entre a party e os oponentes) e state/playerturn.lua (módulo
          responsável pelo turno do personagem, em que ele pode realizar uma
          ação)
        - sons foram adicionados à pasta assets/audio, no formato .ogg, e
          foram colocados nos estados ChooseQuest, Fight (escolha do alvo) e
          PlayerTurn (escolha da ação)
        - foi criado um arquivo state/monster_turn.lua, que implementa o
          estado do turno dos oponentes, que também atacam os personagens da
          party

