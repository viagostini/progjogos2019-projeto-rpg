local love = require 'love'
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local ListMenu = require 'view.list_menu'
local State = require 'state'

local PlayerTurnState = require 'common.class' (State)

local TURN_OPTIONS = { 'Fight', 'Skill', 'Item', 'Run' }

function PlayerTurnState:_init(stack)
  self:super(stack)
  self.action = nil
  self.encounter = nil
  self.character = nil
  self.menu = ListMenu(TURN_OPTIONS)
end

function PlayerTurnState:enter(params)
  self.character = params.current_character
  self.encounter = params.encounter
  self:_show_menu()
  self:_show_cursor()
  self:_show_stats()
end

function PlayerTurnState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, (bfbox.top + bfbox.bottom) / 2)
  self:view():add('turn_menu', self.menu)
end

function PlayerTurnState:_show_cursor()
  local atlas = self:view():get('atlas')
  local sprite_instance = atlas:get(self.character)
  local cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', cursor)
end

function PlayerTurnState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function PlayerTurnState:leave()
  self:view():remove('turn_menu')
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

function PlayerTurnState:resume()
  self:pop()
end

function PlayerTurnState:on_keypressed(key)
  if self.character:get_hp() <= 0 then
    return self:pop({ action = 'Fight', character = self.character })
  end
  local move = love.audio.newSource("assets/audio/Cursor2.ogg", "static")
  move:setVolume(0.4)
  local confirm = love.audio.newSource("assets/audio/Decision3.ogg", "static")
  if key == 'down' then
    move:play()
    self.menu:next()
  elseif key == 'up' then
    move:play()
    self.menu:previous()
  elseif key == 'return' then
    confirm:setVolume(0.35)
    confirm:play()
    local option = TURN_OPTIONS[self.menu:current_option()]
    local status_encounter = false
    for _, v in ipairs(self.encounter) do
      if (v.hp > 0) then
        status_encounter = true
      end
    end
    if option == 'Fight' and status_encounter == true then
      local params = {
        character = self.character,
        encounter = self.encounter
      }
      self.action = option
      return self:push('fight', params)
    elseif status_encounter == false or option == 'Run' then
      return self:pop({ action = option, character = self.character })
    end
  end
end

return PlayerTurnState