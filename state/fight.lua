local love = require 'love'
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local State = require 'state'

local FightState = require 'common.class' (State)

local CRIT_DMG = 2

function FightState:_init(stack)
  self:super(stack)
  self.action = nil
  self.encounter = nil
  self.character = nil
  self.target = nil
end

function FightState:enter(params)
  self.character = params.character
  self.encounter = params.encounter
  self.target = 1
  self:_show_cursor()
  self:_show_stats()
end

function FightState:_show_cursor()
    local atlas = self:view():get('atlas')
    local sprite_instance = atlas:get(self.encounter[self.target])
    local cursor = TurnCursor(sprite_instance)
    self:view():add('fight_cursor', cursor)
end

function FightState:next_target()
    self.target = math.min(#self.encounter, self.target + 1)
    self:_show_cursor()
    self:_show_stats()
end

function FightState:previous_target()
    self.target = math.max(1, self.target - 1)
    self:_show_cursor()
    self:_show_stats()
end

function FightState:current_target()
    return self.target
end

function FightState:leave()
  local target_name = self.encounter[self.target]:get_name()
  local hit_chance = self.character:get_hit()
  local defense = self.encounter[self.target]:get_def()
  if math.random() * 100 <= hit_chance then
    local d = self.character:get_dmg()
    local crit = self.character:get_crit()
    if math.random() * 100 <= crit then
      d = d * CRIT_DMG
    end
    d = d - defense
    if d <= 0 then
      d = 1
    end
    self.encounter[self.target]:change_hp(-d)
    if self.encounter[self.target]:get_hp() <= 0 then
      local message = ("%s killed %s"):format(self.character:get_name(), target_name)
      self:view():get('message'):set(message)
    else
      if d > self.character:get_dmg() then
        local message = ("%s attack was critical!\n\z
          %s took %d critical damage.\n\z
          %s blocked %d damage.\z
          "):format(self.character:get_name(), target_name, d, target_name, defense)
        self:view():get('message'):set(message)
      else
        local message = ("%s deal %d damage to %s\n\z
        %s blocked %d damage.\z
        "):format(self.character:get_name(), d, target_name, target_name, defense)
        self:view():get('message'):set(message)
      end
    end
  else
    local message = ("%s missed the attack on %s"):format(self.character:get_name(), target_name)
    self:view():get('message'):set(message)
  end
  if string.match(self.character:get_name(), "Warrior") then
    local attack = love.audio.newSource("assets/audio/Slash10.ogg", "static")
    attack:play()
  elseif string.match(self.character:get_name(), "Priest") then
    local attack = love.audio.newSource("assets/audio/Blow3.ogg", "static")
    attack:play()
  elseif string.match(self.character:get_name(), "Archer") then
    local attack = love.audio.newSource("assets/audio/Bow1.ogg", "static")
    attack:play()
  end
  self:view():remove('fight_cursor')
end

function FightState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.encounter[self.target])
  self:view():add('char_stats', char_stats)
end

function FightState:on_keypressed(key)
  local move = love.audio.newSource("assets/audio/Cursor2.ogg", "static")
  move:setVolume(0.4)
  local confirm = love.audio.newSource("assets/audio/Decision3.ogg", "static")
  if key == 'return' then
    confirm:setVolume(0.35)
    confirm:play()
    return self:pop()
  elseif key == 'down' then
    move:play()
    self:next_target()
  elseif key == 'up' then
    move:play()
    self:previous_target()
  end
end

return FightState