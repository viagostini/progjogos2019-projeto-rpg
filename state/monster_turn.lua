local love = require 'love'
local State = require 'state'
local MonsterTurnState = require 'common.class' (State)

local CRIT_DMG = 2

function MonsterTurnState:_init(stack)
  self:super(stack)
  self.party = nil
  self.character = nil
end

function MonsterTurnState:enter(params)
  self.character = params.current_character
  self.party = params.party
  return self:pop()
end

function MonsterTurnState:leave()
  if self.character:get_hp() > 0 then
    local attack = love.audio.newSource("assets/audio/Slime.ogg", "static")
    attack:play()
    local target = math.ceil(math.random() * (#self.party))
    local target_name = self.party[target]:get_name()
    local hit_chance = self.character:get_hit()
    local defense = self.party[target]:get_def()
    if math.random() * 100 <= hit_chance then
      local d = self.character:get_dmg()
      local crit = self.character:get_crit()
      if math.random() * 100 <= crit then
        d = d * CRIT_DMG
      end
      d = d - defense
      if d <= 0 then
        d = 1
      end
      self.party[target]:change_hp(-d)
      if self.party[target]:get_hp() <= 0 then
        local message = ("%s killed %s"):format(self.character:get_name(), target_name)
        self:view():get('message'):set(message)
      else
        if d > self.character:get_dmg() then
          local message = ("%s attack was critical!\n\z
            %s took %d critical damage.\n\z
            %s blocked %d damage.\z
            "):format(self.character:get_name(), target_name, d, target_name, defense)
          self:view():get('message'):set(message)
        else
          local message = ("%s deal %d damage to %s\n\z
          %s blocked %d damage.\z
          "):format(self.character:get_name(), d, target_name, target_name, defense)
          self:view():get('message'):set(message)
        end
      end
    else
      local message = ("%s missed the attack on %s"):format(self.character:get_name(), target_name)
      self:view():get('message'):set(message)
    end
  end
end

return MonsterTurnState
