
return {
  name = "Recluse Archer",
  appearance = 'archer',
  max_hp = 12,
  damage = 4,
  defense = 1,
  hit = 86,
  crit = 12,
  speed = 100
}

