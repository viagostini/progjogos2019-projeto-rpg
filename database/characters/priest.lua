
return {
  name = "Friendly Priest",
  appearance = 'priest',
  max_hp = 8,
  damage = 3,
  defense = 3,
  hit = 94,
  crit = 6,
  speed = 24
}

