
return {
  name = "Green Slime",
  appearance = 'slime',
  max_hp = 16,
  damage = 1,
  defense = 1,
  hit = 92,
  crit = 2,
  speed = 1,
}

