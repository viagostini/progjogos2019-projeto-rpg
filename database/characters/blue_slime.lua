
return {
  name = "Blue Slime",
  appearance = 'blue_slime',
  max_hp = 25,
  damage = 3,
  defense = 2,
  hit = 95,
  crit = 3,
  speed = 2,
}

