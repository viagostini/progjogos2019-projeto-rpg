
return {
  name = "Veteran Warrior",
  appearance = 'knight',
  max_hp = 20,
  damage = 4,
  defense = 2,
  hit = 95,
  crit = 9,
  speed = 8
}

